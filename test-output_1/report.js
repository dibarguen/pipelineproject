$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/com/grupoaval/portalnuevastecnologias/features/PasarelaDePago.feature");
formatter.feature({
  "name": "Agregar producto al carrito de compras",
  "description": "  Como usuario dentro del sistema, quiero adicionar productos al carrito de compras",
  "keyword": "Característica"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Antecedentes"
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona la tecla Enter",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaLaTeclaEnter()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "validar boton pse",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "selecciona una categoria",
  "rows": [
    {
      "cells": [
        "audio"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccioneCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el siguiente mensaje:",
  "rows": [
    {
      "cells": [
        "El producto Celular XIAOMI Mi A2 Lite DS 4G Dorado ha sido añadido al carrito."
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarPopupProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "escoge otra categoria",
  "rows": [
    {
      "cells": [
        "celulares"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionarOtraCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Audífonos Sony tipo banda para la cabeza - MDR-ZX110"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el siguiente mensaje:",
  "rows": [
    {
      "cells": [
        "El producto Audífonos inalámbricos EXTRA BASS™ XB650BT ha sido añadido al carrito."
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarPopupProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el producto agregado en el apartado de items del carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.vadidarProductoItem()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se ingresa al menu de Cuenta",
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.cickMenuCuenta()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona ver carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.clickVerCarrito()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el producto agregado:",
  "rows": [
    {
      "cells": [
        "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
      ]
    },
    {
      "cells": [
        "Audífonos inalámbricos EXTRA BASS™ XB650BT"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona el boton proceder al pago",
  "keyword": "Cuando "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.procederAlPago()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "procede a realizar el check out, llenando un formulario",
  "rows": [
    {
      "cells": [
        "Nombre",
        "Apellido",
        "segundoApellido",
        "Compania",
        "Correo",
        "Telefono",
        "Pais",
        "Ciudad",
        "Provincia",
        "Postal",
        "Direccion"
      ]
    },
    {
      "cells": [
        "Dilan",
        "Ibarguen",
        "Garcia",
        "SQA S.A",
        "dilan.ibarguen@sqasa.co",
        "652323231",
        "Angola",
        "Bogota",
        "Cundinamarca",
        "100111",
        "cra. 8 No.64-42"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.llenarDatosFormulario(Pasarela\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "pulsa el boton siguiente valida que se dirija a Detalle de orden",
  "keyword": "Entonces "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.continuarPagando()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que se dirija al siguiente formulario Metodo de pago",
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.confirmarPedido()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "escoge el metodo de pago Debito PSE",
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.metodoDePago()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Antecedentes"
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona la tecla Enter",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaLaTeclaEnter()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Pagar productos seleccionados",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "selecciona una categoria",
  "rows": [
    {
      "cells": [
        "audio"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccioneCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el siguiente mensaje:",
  "rows": [
    {
      "cells": [
        "El producto Celular XIAOMI Mi A2 Lite DS 4G Dorado ha sido añadido al carrito."
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarPopupProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "escoge otra categoria",
  "rows": [
    {
      "cells": [
        "celulares"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionarOtraCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Audífonos Sony tipo banda para la cabeza - MDR-ZX110"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el siguiente mensaje:",
  "rows": [
    {
      "cells": [
        "El producto Audífonos inalámbricos EXTRA BASS™ XB650BT ha sido añadido al carrito."
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarPopupProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el producto agregado en el apartado de items del carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.vadidarProductoItem()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se ingresa al menu de Cuenta",
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.cickMenuCuenta()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona ver carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.clickVerCarrito()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el producto agregado:",
  "rows": [
    {
      "cells": [
        "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
      ]
    },
    {
      "cells": [
        "Audífonos inalámbricos EXTRA BASS™ XB650BT"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona el boton proceder al pago",
  "keyword": "Cuando "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.procederAlPago()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "procede a realizar el check out, llenando un formulario",
  "rows": [
    {
      "cells": [
        "Nombre",
        "Apellido",
        "segundoApellido",
        "Compania",
        "Correo",
        "Telefono",
        "Pais",
        "Ciudad",
        "Provincia",
        "Postal",
        "Direccion"
      ]
    },
    {
      "cells": [
        "Dilan",
        "Ibarguen",
        "Garcia",
        "SQA S.A",
        "dilan.ibarguen@sqasa.co",
        "652323231",
        "Angola",
        "Bogota",
        "Cundinamarca",
        "100111",
        "cra. 8 No.64-42"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.llenarDatosFormulario(Pasarela\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "pulsa el boton siguiente valida que se dirija a Detalle de orden",
  "keyword": "Entonces "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.continuarPagando()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que se dirija al siguiente formulario Metodo de pago",
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.confirmarPedido()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "escoge el metodo de pago Debito PSE",
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.metodoDePago()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que se genere formulario PSE",
  "rows": [
    {
      "cells": [
        "Banco"
      ]
    },
    {
      "cells": [
        "Banco de Occidente"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.seleccionaBanco(Pasarela\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa tipo de persona y nombre de titular",
  "rows": [
    {
      "cells": [
        "Persona",
        "Titular"
      ]
    },
    {
      "cells": [
        "Natural",
        "Dilan"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.ingresarTipo(Pasarela\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona tipo de documento y numero de documento",
  "rows": [
    {
      "cells": [
        "Tipo",
        "Numero"
      ]
    },
    {
      "cells": [
        "CE",
        "3242342"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.tipoDocumentoNumero(Pasarela\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa correo y telefono",
  "rows": [
    {
      "cells": [
        "Email",
        "Telefono"
      ]
    },
    {
      "cells": [
        "dilan.ibarguen@sqasa.co",
        "65312122"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.ingresarCorreoTelefono(Pasarela\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "pulsa el boton realizar pago",
  "keyword": "Entonces "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.botonRealizarPago()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se genera un resultado del pago con la informacion diligenciada",
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.informacionDiligenciada()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "hacemos clic en el boton finalizar compra",
  "keyword": "Entonces "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.botonFinalizarCompra()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se valida que la orden se ejecuto correctamente",
  "keyword": "Y "
});
formatter.match({
  "location": "PasarelaDePagoStepDefinitions.validarOrden()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Antecedentes"
});
